class AddPartNameToParts < ActiveRecord::Migration[6.0]
  def change
    add_column :parts, :part_name, :string
  end
end
