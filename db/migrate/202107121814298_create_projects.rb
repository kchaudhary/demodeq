class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
      t.references :programmer, null: false, foreign_key: true
      t.references :client, null: false, foreign_key: true
      t.date :project_date

      t.timestamps
    end
  end
end
