class CreateAdminRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_roles do |t|
      t.integer :role_type
      t.text :description

      t.timestamps
    end
  end
end
