class AddStudentToBook < ActiveRecord::Migration[6.0]
  def change
    add_reference :books, :student, null: true, foreign_key: true
  end
end
