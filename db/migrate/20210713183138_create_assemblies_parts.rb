class CreateAssembliesParts < ActiveRecord::Migration[6.0]
  def change
    create_table :assemblies_parts do |t|
      t.references :assembly, null: false, foreign_key: true
      t.references :part, null: false, foreign_key: true
    end
  end
end
