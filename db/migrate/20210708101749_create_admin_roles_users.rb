class CreateAdminRolesAdminUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_roles_users do |t|
      t.references :admin_user, null: false, foreign_key: true
      t.references :admin_role, null: false, foreign_key: true
    end
  end
end
