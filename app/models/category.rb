class Category < ApplicationRecord
  after_save :clear_cache

  def clear_cache
    $redis.del "categories"
  end
  
end