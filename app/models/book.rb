class Book < ApplicationRecord
  belongs_to :user
  belongs_to :student
  validates :description,  presence: true 
end
