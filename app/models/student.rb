class Student < ApplicationRecord
  has_many :books
  validates :name, :roll_number, presence: true 
end
