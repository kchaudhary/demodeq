class AdminRole < ApplicationRecord
  
  has_and_belongs_to_many :admin_users

  enum role_type:[:user,:admin,:superadmin]

end
