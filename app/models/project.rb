class Project < ApplicationRecord
  belongs_to :programmer
  belongs_to :client
  accepts_nested_attributes_for :programmer
end
