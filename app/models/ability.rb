class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= AdminUser.new
    if user.admin_roles.map{|m| m.superadmin? }.first
      can :manage, :all
    elsif user.admin_roles.map{|m| m.admin? }.first
      can :read, :update, :all 
    else
      can :read, :all
    end
  end
end