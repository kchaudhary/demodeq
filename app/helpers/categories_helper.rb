module CategoriesHelper
  def fetch_categories
    categories =  $redis.get("categories")
    if categories.nil?
      categories = Category.all.to_json
      $redis.set("categories", categories)
      # Expire the cache, every 200 seconds.
      $redis.expire("categories", 200.seconds.to_i)
    end
    @categories = JSON.load categories
  end
end

