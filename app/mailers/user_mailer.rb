class UserMailer < ApplicationMailer
  default from: Figaro.env.SENDMAIL_EMAIL

  def birthday_email(user)
    @user = user
    mail(to: @user.email, subject: 'Birthday Wishes')
  end
end
