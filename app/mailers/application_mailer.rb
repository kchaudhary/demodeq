class ApplicationMailer < ActionMailer::Base
  default from: Figaro.env.SENDMAIL_EMAIL
  layout 'mailer'
end
