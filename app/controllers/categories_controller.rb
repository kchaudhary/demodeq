class CategoriesController < ApplicationController
  include CategoriesHelper

  def index
    @categories = fetch_categories
  end

  def new
  end
  
end
