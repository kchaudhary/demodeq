require 'nokogiri'
require 'open-uri'

class HomesController < ApplicationController
  def welcome 
  end 

  def parsing_data
    doc = Nokogiri::HTML(URI.open('https://nokogiri.org/tutorials/installing_nokogiri.html'))
    @supported_platform = doc.css('article ul li')[1..4]
    @native_gem_details = doc.xpath('//article//p')[8..10]
    @nokogiri_support = doc.search('nav ul li', '//label//a')
  end 

end
