class ClientsController < ApplicationController

  def index
    @clients = Client.all 
  end

  def new
    @client = Client.new 
    @projects = @client.projects.build
    @programmer = @projects.build_programmer 
  end

  def create
    client = Client.new(client_params)
    if client.save
      redirect_to root_path
    else 
      redirect_to clients_new_path
    end
  end

  private
  def client_params
    params.require(:client).permit(:id, :name, projects_attributes: [:id, :project_date, programmer_attributes: [:id, :name ]] )
  end
end