class PartsController < ApplicationController

  def index
    @parts = Part.all
  end

  def show
    @part = Part.find(params[:id])
  end

  def create
    @part = Part.new(part_params)
    if @part.save
      redirect_to parts_index_path
    else 
      redirect_to parts_new_path
    end
  end

  def new
    @part = Part.new
    @assemblies = @part.assemblies.build
  end

  private
  def part_params
    params.require(:part).permit(:id, :part_number, :part_name, assemblies_attributes: [:id, :name] )
  end

end
