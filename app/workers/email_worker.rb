class EmailWorker
  include Sidekiq::Worker

  def perform(*args)
    user = User.where(birthday: Date.today)
    user.each do |user| 
      UserMailer.birthday_email(user).deliver_now
    end 
  end
end
