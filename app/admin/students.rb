ActiveAdmin.register Student do

  permit_params :name, :age, :class_name, :roll_number

  index do
    selectable_column
    id_column
    column :name
    column :age
    column :class_name
    column :roll_number
    actions
  end

  filter :name
  filter :age
  filter :class_name
  filter :roll_number

  form do |f|
    f.inputs do
      f.input :name
      f.input :age
      f.input :class_name
      f.input :roll_number
    end
    f.actions
  end
end
