ActiveAdmin.register Book do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :description, :price, :user_id, :student_id
  filter :user_id, as: :select, collection: proc {User.all.map{|i| [i.email, i.id]}}
  filter :student_id, as: :select, collection: proc {Student.all.map{|i| [i.name, i.id]}}
  form html: { enctype: "multipart/form-data" } do |f|  
    f.inputs "Books" do
      f.input :user_id, label: 'User', as: :select, collection: User.all.map{|m| [m.email,m.id]}, include_blank: false
      f.input :description, label: 'Description', as: :string
      f.input :price, label: 'Price', as: :number
      f.input :student_id, label: 'Student', as: :select, collection: Student.all.map{|m| [m.name,m.id]}, include_blank: false
      f.actions
    end
  end
  member_action :price, method: :put do 
    book = Book.find(params[:id])
    book.update(price: 10000)
    redirect_to admin_book_path(book)
  end 

  controller do
  # This code is evaluated within the controller class
    def details
      @book = Book.find(params[:id])
    end
  end
  collection_action :price_upto_hundred, method: :get do
    @books = Book.where("price <= ?", 100)
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:description, :price, :user_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
end
